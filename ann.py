# -*- coding: utf-8 -*-


from google.colab import files
uploaded = files.upload()

import io
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
df = pd.read_csv(io.BytesIO(uploaded['Churn_Modelling.csv']))

"""Now we're going to learn more about data and preprocess it"""

df.head()

X = df.iloc[:,3:13]
y = df.iloc[:,13].values
print(X.shape)
print(y.shape)
print(type(X))
print(type(y))

cats = ['Geography','Gender']
X = pd.get_dummies(X,columns=cats,drop_first=True)

X

X=X.values

"""# Part 2 : Seperating train and test"""

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

"""# Part 3 : Transforming the Data"""

from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

"""# Part 4 : Building the Artificial Neural Network"""

import keras
from keras.models import Sequential
from keras.layers import Dense

classifier = Sequential()

classifier.add(Dense(units=6,activation='relu',kernel_initializer='uniform',input_dim=11))
classifier.add(Dense(units=6,activation='relu',kernel_initializer='uniform'))
classifier.add(Dense(units=1,activation='sigmoid',kernel_initializer='uniform'))

classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
classifier.fit(X_train, y_train, batch_size = 10, epochs = 100)

y_pred = classifier.predict(X_test)

y_pred

gone = np.count_nonzero(y)
print(gone)

y_pred = (y_pred > 0.5)

from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

print(cm)

class_names=[0,1] # name  of classes
fig, ax = plt.subplots()
tick_marks = np.arange(len(class_names))
plt.xticks(tick_marks, class_names)
plt.yticks(tick_marks, class_names)
# create heatmap
sns.heatmap(pd.DataFrame(cm), annot=True, cmap="YlGnBu" ,fmt='g')
ax.xaxis.set_label_position("top")
plt.tight_layout()
plt.title('Confusion matrix', y=1.1)
plt.ylabel('Actual label')
plt.xlabel('Predicted label')

y_pred_proba = classifier.predict(X_test)
from sklearn.metrics import precision_recall_curve
from sklearn import metrics

precision, recall, thresholds = precision_recall_curve(y_test, y_pred_proba) 
pr_auc = metrics.auc(recall, precision)

plt.title("Precision-Recall vs Threshold Chart")
plt.plot(thresholds, precision[: -1], "b--", label="Precision")
plt.plot(thresholds, recall[: -1], "r--", label="Recall")
plt.ylabel("Precision, Recall")
plt.xlabel("Threshold")
plt.legend(loc="lower left")
plt.ylim([0,1])

print(pr_auc)

"""# Making a Single Prediction"""

new_pred = classifier.predict(sc.transform(np.array([[600,40,3,60000,2,1,1,50000,0,0,1]])))
new_pred = (new_pred > 0.5)
print(new_pred)

"""# Evaluating ANN"""

from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score

"""defining our classifier function:"""

def build_classifier():
  classifier = Sequential()
  classifier.add(Dense(units=6,activation='relu',kernel_initializer='uniform',input_dim=11))
  classifier.add(Dense(units=6,activation='relu',kernel_initializer='uniform'))
  classifier.add(Dense(units=1,activation='sigmoid',kernel_initializer='uniform'))
  classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
  
  return classifier

my_classifier = KerasClassifier(build_fn=build_classifier,batch_size = 10, epochs = 100)
accuracies = cross_val_score(estimator=my_classifier,X=X_train,y=y_train,
                            cv=10,n_jobs=-1)

print(accuracies)

mean = np.mean(accuracies)
variance = np.std(accuracies)
print(mean)
print(variance)

"""# Drop Out Regularization

as we saw from variance, we dont have an overfitting problem here
but sometimes the variance is much bigger than this and there is an overfitting problem. due to solving that problem we use drop out
"""

from keras.layers import Dropout

classifier_d = Sequential() #with drop out
classifier_d.add(Dense(units=6,activation='relu',kernel_initializer='uniform',input_dim=11))
classifier_d.add(Dropout(rate=0.1))
classifier_d.add(Dense(units=6,activation='relu',kernel_initializer='uniform'))
classifier_d.add(Dropout(rate=0.1))
classifier_d.add(Dense(units=1,activation='sigmoid',kernel_initializer='uniform'))
classifier_d.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

classifier_d.fit(X_train, y_train, batch_size = 10, epochs = 100)
y_pred_d = classifier_d.predict(X_test)
y_pred_d = (y_pred_d > 0.5)

acc_d = metrics.accuracy_score(y_test,y_pred_d)
print(acc_d)

"""# Parameter Tuning"""

from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import GridSearchCV

def build_classifier(optimizer):
  classifier = Sequential()
  classifier.add(Dense(units=6,activation='relu',kernel_initializer='uniform',input_dim=11))
  classifier.add(Dense(units=6,activation='relu',kernel_initializer='uniform'))
  classifier.add(Dense(units=1,activation='sigmoid',kernel_initializer='uniform'))
  classifier.compile(optimizer = optimizer, loss = 'binary_crossentropy', metrics = ['accuracy'])
  
  return classifier

my_classifier_t = KerasClassifier(build_fn=build_classifier)
my_classifier = KerasClassifier(build_fn=build_classifier,batch_size = 10, epochs = 100)
parameters = {'batch_size' : [25,32],
             'epochs' : [100,500],
             'optimizer' : ['adam','rmsprop']}

grid_search = GridSearchCV(estimator=my_classifier_t,param_grid=parameters,
                          scoring = 'accuracy',cv=10)

grid_search = grid_search.fit(X_train,y_train)
best_params = grid_search.best_params_

best_acc = grid_search.best_score_
print(best_acc)
print(best_params)

